#pragma once

#include <odin/md/Book.hpp>
#include <odin/md/MktDataListener.hpp>

#include <odin/t/MktState.hpp>
#include <odin/t/Money.hpp>
#include <odin/t/OrdSide.hpp>
#include <odin/t/Qty.hpp>

#include <iostream>


class MDP3_MktDataListener
    : public odin::md::MktDataListener
{
public:

    MDP3_MktDataListener(std::ostream & os = std::cout);

    virtual void onBookUpdate(odin::md::Book const &) override;
    virtual void onEndMessages() override;
    virtual void onTrade(odin::t::Qty const, odin::t::Money const, odin::t::OrdSide const) override;
    virtual void onSecurityTradingStatusChange(odin::t::MktState const) override;
    virtual void onPreviousClosingPrice(odin::t::Money const) override;
    virtual void onIndicativeOpeningPrice(odin::t::Money const) override;

    std::ostream & os() const;

private:

    std::ostream & m_os;
};

inline
std::ostream &
MDP3_MktDataListener::
os() const
{
    return m_os;
}

