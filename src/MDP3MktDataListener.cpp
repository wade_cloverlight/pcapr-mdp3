#include "MDP3MktDataListener.hpp"

#include <odin/md/mdp3/schema/Schema.hpp>


MDP3_MktDataListener::
MDP3_MktDataListener(std::ostream & os)
    : m_os(os)
{
}

void
MDP3_MktDataListener::
onBookUpdate(odin::md::Book const & a_book)
{
    m_os << a_book << std::endl;
}

void
MDP3_MktDataListener::
onEndMessages()
{
}

void
MDP3_MktDataListener::
onTrade(odin::t::Qty const, odin::t::Money const, odin::t::OrdSide const)
{
}

void
MDP3_MktDataListener::
onSecurityTradingStatusChange(odin::t::MktState const)
{
}

void
MDP3_MktDataListener::
onPreviousClosingPrice(odin::t::Money const)
{
}

void
MDP3_MktDataListener::
onIndicativeOpeningPrice(odin::t::Money const)
{
}

