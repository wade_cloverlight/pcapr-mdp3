//#include "VSMMktDataListener.hpp"

//#include "MDP3MktDataListener.hpp"
//#include "MDP3MktDataListenerFactory.hpp"

#include <odin/md/mdp3/feed/MessageHandlerStats.hpp>
#include <odin/md/mdp3/feed/MessageWriter.hpp>
#include <odin/pc/PCAP_IOState.hpp>
#include <odin/pc/PCAP_UDP_IFStream.hpp>
#include <odin/u/LogCout.hpp>

/*
#include <odin/md/vsm/BookingHandler.hpp>
#include <odin/md/vsm/InstrumentStore.hpp>
#include <odin/md/vsm/MessageHandler.hpp>
#include <odin/md/vsm/MessageWriter.hpp>
#include <odin/md/vsm/Packet.hpp>
#include <odin/md/vsm/Schema.hpp>
#include <odin/md/vsm/SchemaOStream.hpp>
#include <odin/t/InstrmtDefs.hpp>
#include <odin/t/InstrmtDefsLoader.hpp>
*/

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>

#include <arpa/inet.h>
#include <netinet/in.h>

#include <cstdint>
#include <fstream>
#include <iterator>
#include <iostream>
#include <string>
#include <vector>


int
usage(boost::program_options::options_description const & a_desc,
        std::string const & a_err = "")
{
    static const char * const App = "PCAP Reader for CME MDP3";
    static const char * const Version = "0.1";

    std::cerr << "\n" << App << " " << Version << "\n\n";
    if (not a_err.empty())
    {
        std::cerr << "\nProgram arguments error: " << a_err << "\n";
    }
    std::cerr << "Usage: " << a_desc << "\n";
    return 1;
}

/*
TemplateIDConverter::TemplateIDList
get_template_id_list_from_msgs(std::string const & a_msgs, std::string const & a_all_msgs_option_value = "")
{
    TemplateIDConverter template_id_converter;

    vector<std::string> msg_list;
    boost::split(msg_list, a_msgs, boost::is_any_of(","));

    // Use all template IDs if any option is "All".
    auto found = std::find(std::begin(msg_list), std::end(msg_list), a_all_msgs_option_value);
    if (not a_all_msgs_option_value.empty() && found != std::end(msg_list))
    {
        return template_id_converter.template_id_list();
    }

    // Convert each template name to ID.
    TemplateIDConverter::TemplateIDList template_id_list;
    for (auto && msg : msg_list)
    {
        template_id_list.emplace_back(template_id_converter.name_to_template_id(msg));
    }

    // Skip invalid IDs.
    template_id_list.erase(
        std::remove(
            std::begin(template_id_list),
            std::end(template_id_list),
            TemplateID::Invalid
            //[](TemplateID templateID){ return templateID == TemplateID::Invalid; } // Trying out lambda with remove_if
        ),
        std::end(template_id_list)
    );

    return template_id_list;
}

void
exclude_template_ids(TemplateIDConverter::TemplateIDList const & a_exclude_template_id_list, TemplateIDConverter::TemplateIDList & a_template_id_list)
{
    // Remove all templateIDs from a_template_id_list that are in a_exclude_template_id_list.
    for (auto && templateID : a_exclude_template_id_list)
    {
        a_template_id_list.erase(
            std::remove(
                std::begin(a_template_id_list),
                std::end(a_template_id_list),
                templateID
            ),
            std::end(a_template_id_list)
        );
    }
}
*/

// Read and handle packets in a pcap file.
// input_fname : Name of pcap file
// begin_pkt : First packet to handle
// pkt_cnt : Number of packets to handle
// msg_handler : Object to handle each read packet
// error : Error string if there is an error reading a packet
// ignore_packet_errors : Whether to ignore errors when reading packets
// Return IO state.
template <typename MessageHandlerImpl>
odin::pc::PCAP_IOState
read_pcap(std::string const & input_fname
        , size_t const begin_pkt
        , size_t const pkt_cnt
        , MessageHandlerImpl & msg_handler
        , std::string & error
        , bool const ignore_packet_errors
        , bool const print_newline_after_packet
        , bool const verbose
        )
{
    odin::pc::PCAP_UDP_IFStream ifs{input_fname};
    odin::pc::UDP_Packet pkt{};
    odin::pc::PCAP_IOState ios = ifs.iostate();

    if (begin_pkt != 0)
    {
        // Read up to begin_pkt.
        for (size_t n = 0; n < begin_pkt && ((ios = ifs.read_udp(pkt)) == odin::pc::PCAP_IOState::Good); ++n)
        {
        }
    }

    if (ios == odin::pc::PCAP_IOState::Error)
    {
        error = ifs.error();
        return ios;
    }

    /*
    //std::string required_dst_str = "224.0.31.151"; // Snapshot A
    std::string required_dst_str = "224.0.31.130"; // Incremental A
    //std::string required_dst_str = "224.0.32.151"; // Snapshot B
    //std::string required_dst_str = "224.0.32.130"; // Incremental B
    uint16_t port = 14382; // Snapshot
    //uint16_t port = 15382; // Incremental
    */

    // Channel 344
    //std::string required_dst_str = "224.0.31.89"; // Snapshot A
    //std::string required_dst_str = "224.0.31.68"; // Incremental A
    //std::string required_dst_str = "224.0.32.89"; // Snapshot B
    //std::string required_dst_str = "224.0.32.68"; // Incremental B
    //uint16_t port = 14344; // A
    //uint16_t port = 15344; // B

    // Prod - ES - Channel 310
    //std::string required_dst_str = "233.158.8.43"; // Snapshot A
    //std::string required_dst_str = "233.158.8.1"; // Incremental A
    //std::string required_dst_str = "233.158.8.170"; // Snapshot B
    //std::string required_dst_str = "233.158.8.128"; // Incremental B
    //uint16_t port = 6310; // Snapshot A
    //uint16_t port = 14310; // Incremental A
    //uint16_t port = 7310; // Snapshot B
    //uint16_t port = 15310; // Incremental B

    // Test addr.
    std::string required_dst_str = "224.0.32.1";
    uint16_t port = 15310;

    in_addr addr{};
    inet_aton(required_dst_str.c_str(), &addr);

    // Read and handle each packet.
    size_t ignored_packet_error_count = 0;
    for (size_t n = 0; (pkt_cnt == 0 or n < pkt_cnt) and ((ios = ifs.read_udp(pkt)) == odin::pc::PCAP_IOState::Good); ++n)
    {
        // Must copy because inet_ntoa returns pointer to a static buffer.
        std::string const ip_src_str = inet_ntoa(pkt.ip_src);
        std::string const ip_dst_str = inet_ntoa(pkt.ip_dst);
        if (verbose)
        {
            LOG_COUT("src=" << ip_src_str << ":" << pkt.port_src
                    << ", dst=" << ip_dst_str << ":" << pkt.port_dst
                    << ", pktnum=" << (n + 1)
                    );
        }

        //bool const are_addrs_equal = (pkt.ip_dst.s_addr == addr.s_addr);
        bool const are_addrs_equal = (ip_dst_str == required_dst_str);
        bool const are_ports_equal = (pkt.port_dst == port);
        //bool const skip_pkt = not are_addrs_equal;
        //bool const skip_pkt = not are_ports_equal;
        bool const skip_pkt = not (are_addrs_equal and are_ports_equal);
        if (verbose)
        {
            LOG_COUT("skip_pkt=" << skip_pkt << ": ip_dst_str=" << ip_dst_str << ", required_dst_str=" << required_dst_str
                    << ": are_addrs_equal=" << are_addrs_equal << ", are_ports_equal=" << are_ports_equal
                    );
        }
        //bool const skip_pkt = false;
        if (skip_pkt)
        {
            continue;
        }
        if (verbose)
        {
            LOG_COUT("not skipped: src=" << ip_src_str << ":" << pkt.port_src
                    << ", dst=" << ip_dst_str << ":" << pkt.port_dst
                    << ", pktnum=" << (n + 1)
                    );
        }

        msg_handler.handle(pkt);

        if (print_newline_after_packet)
        {
            std::cout << std::endl;
        }
    }

    if (ios == odin::pc::PCAP_IOState::Error)
    {
        error = ifs.error();
    }

    if (ignore_packet_errors and ignored_packet_error_count != 0)
    {
        std::cerr << "ignored_packet_error_count: " << ignored_packet_error_count << std::endl;
    }

    return ios;
}

int main(int a_c, char *a_v[])
{
    std::ios_base::sync_with_stdio(false);

    // Setup the --msgs program option.
    //TemplateIDConverter template_id_converter;
    //std::string const msgNames = boost::algorithm::join(template_id_converter.template_name_list(), ", ");
    //std::string const msgsOptionDesc = "Comma-separated list of message types to print. Use 'All' for all messages. Choose from: " + msgNames;
    //static std::string const allMsgsOptionValue = "All";

    boost::program_options::options_description desc{"Options"};
    desc.add_options()
        ("help,h", "This help message.")
        ("input,i", boost::program_options::value<std::string>(), "The PCAP input file to read. Use '-' for stdin.")
        ("begin,b", boost::program_options::value<size_t>()->default_value(0), "First packet to read.")
        ("count,n", boost::program_options::value<size_t>()->default_value(0), "Number of packets to read after the first.")
        //("msgs,m", boost::program_options::value<std::string>()->default_value(allMsgsOptionValue), msgsOptionDesc.c_str())
        ("exclude-msgs,x", boost::program_options::value<std::string>()->default_value(""), "Comma-separated list of message types to exclude from the msgs list. Useful for '--msgs All'")
        ("product-ids,p", boost::program_options::value<std::string>()->default_value(""), "Comma-separated list of product IDs for which to print.")
        ("security-ids,s", boost::program_options::value<std::string>()->default_value(""), "Comma-separated list of security IDs (aka instrument IDs) for which to print (only valid if '--book yes').")
        ("book,B", boost::program_options::value<bool>()->default_value(false), "Whether to print the order book.")
        ("stats,S", boost::program_options::value<bool>()->default_value(false), "Whether to print message statistics.")
        ("instruments-file,I", boost::program_options::value<std::string>()->default_value(""), "Instruments file.")
        ("last-seq-no,Q", boost::program_options::value<std::string>()->default_value(""), "Last message sequence number for each product, so only messages with a sequence number > this number are handled (only valid if '--book yes').")
        ("ignore-packet-errors,E", boost::program_options::value<bool>()->default_value(false), "Whether to ignore packets that the EOBI msg handler could not handle. Useful if the pcap contains a mix of EOBI and other packet types.")
        ("print-newline-after-packet,N", boost::program_options::value<bool>()->default_value(false), "Whether to print a newline after each handled packet.")
        ("verbose,v", boost::program_options::value<bool>()->default_value(false), "Whether to print more verbose output.")
        ;

    boost::program_options::variables_map vm{};
    try
    {
        boost::program_options::store(boost::program_options::parse_command_line(a_c, a_v, desc), vm);
        boost::program_options::notify(vm);
        if (vm.count("help") or not vm.count("input"))
        {
            return usage(desc);
        }
    }
    catch (boost::program_options::error const & ex)
    {
        return usage(desc, ex.what());
    }
    std::string const & input_fname = vm["input"].as<std::string>();
    size_t const pkt_cnt = vm["count"].as<size_t>();
    size_t const begin_pkt = vm["begin"].as<size_t>();
    //std::string const & msgs = vm["msgs"].as<std::string>();
    //std::string const & exclude_msgs = vm["exclude-msgs"].as<std::string>();
    //std::string const & product_ids = vm["product-ids"].as<std::string>();
    //std::string const & security_ids = vm["security-ids"].as<std::string>();
    bool const stats = vm["stats"].as<bool>();
    //bool const book = vm["book"].as<bool>();
    //std::string const & instruments_file = vm["instruments-file"].as<std::string>();
    //std::string const & last_seq_no_str = vm["last-seq-no"].as<std::string>();
    bool const ignore_packet_errors = vm["ignore-packet-errors"].as<bool>();
    bool const print_newline_after_packet = vm["print-newline-after-packet"].as<bool>();
    bool const verbose = vm["verbose"].as<bool>();

    // Load instrument definitions and store them in the security store so they can be subscribed to.
    /*
    InstrmtDefs instrument_defs;
    if (not instruments_file.empty())
    {
        load_instrmtdefs(instruments_file, instrument_defs);
        SecurityDefinitionStoreSingleton::instance().store(instrument_defs);
    }
    */

    odin::pc::PCAP_IOState ios{};
    std::string error{};
    /*
    if (book)
    {
        if (instruments_file.empty())
        {
            std::cerr << "Error: Instruments file required when running book mode" << std::endl;
            return EXIT_FAILURE;
        }

        auto book_handler = std::make_shared<BookingHandler>();
        MessageHandler book_writer(book_handler,
                SecurityDefinitionStoreSingleton::instance()
                );

        // Security IDs are in a comma-separated string.
        vector<std::string> security_id_str_list;
        boost::split(security_id_str_list, security_ids, boost::is_any_of(","));

        // Subscribe to all securities if none are specified.
        // boost::split will create a container holding only "" if the string was empty.
        bool subscribe_all = std::all_of(std::begin(security_id_str_list), std::end(security_id_str_list),
                [&](const std::string & s){return s.empty();});

        if (subscribe_all)
        {
            auto listener_factory = std::make_shared<EOBI_MktDataListenerFactory>(std::cout);
            book_writer.subscribe_all(listener_factory);
        }
        else
        {
            auto listener = std::make_shared<EOBI_MktDataListener>(std::cout);

            // Subscribe to each security ID.
            for (auto && security_id_str : security_id_str_list)
            {
                if (security_id_str.empty())
                {
                    continue;
                }

                // Allow both a numeric ID like 1062785 or the corresponding symbol name like FESX201509.
                try
                {
                    const SecurityID security_id = lexical_cast<SecurityID>(security_id_str);
                    book_writer.subscribe(security_id, listener);
                }
                catch (boost::bad_lexical_cast const &)
                {
                    book_writer.subscribe(security_id_str, listener);
                }
            }
        }

        // Must invalidate all last msg seq no or else will crash if get an order delete from an incremental msg that's not in the book yet.
        for (auto && product_id : book_writer.product_ids_with_subscriptions())
        {
            book_writer.invalidate_last_msg_seq_num_processed(product_id);
        }

        if (not last_seq_no_str.empty())
        {
            auto last_seq_no = lexical_cast<SeqNum>(last_seq_no_str);
            for (auto && product_id : book_writer.product_ids_with_subscriptions())
            {
                book_writer.set_last_msg_seq_num_processed(product_id, last_seq_no);
            }
        }

        ios = read_pcap(input_fname, begin_pkt, pkt_cnt, book_writer, error, ignore_packet_errors, print_newline_after_packet);
    }
    */
    //else
    if (stats)
    {
        odin::md::mdp3::MessageHandlerStats msg_handler_stats{};
        ios = read_pcap(input_fname, begin_pkt, pkt_cnt, msg_handler_stats, error, ignore_packet_errors, print_newline_after_packet, verbose);
        std::cout << msg_handler_stats << std::endl;
    }
    else
    {
    /*
        TemplateIDConverter::TemplateIDList template_id_list = get_template_id_list_from_msgs(msgs, allMsgsOptionValue);
        const TemplateIDConverter::TemplateIDList exclude_template_id_list = get_template_id_list_from_msgs(exclude_msgs, allMsgsOptionValue);
        exclude_template_ids(exclude_template_id_list, template_id_list);

        // Get list of product IDs.
        vector<std::string> product_id_str_list;
        boost::split(product_id_str_list, product_ids, boost::is_any_of(","));
        vector<MarketSegmentID> product_id_list;
        for (auto && product_id_str : product_id_str_list)
        {
            if (product_id_str.empty())
            {
                continue;
            }
            product_id_list.emplace_back(lexical_cast<MarketSegmentID>(product_id_str));
        }
    */

        odin::md::mdp3::MessageWriter msg_writer{std::cout};
        ios = read_pcap(input_fname, begin_pkt, pkt_cnt, msg_writer, error, ignore_packet_errors, print_newline_after_packet, verbose);
    }

    if (ios == odin::pc::PCAP_IOState::Error)
    {
        std::cerr << "Error: " << error << std::endl;
        return 1;
    }
    else if (ios == odin::pc::PCAP_IOState::Unknown)
    {
        std::cerr << "Unknown IO state!" << std::endl;
        return 2;
    }
    return 0;
}
