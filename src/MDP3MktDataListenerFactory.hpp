#pragma once

#include <odin/md/MktDataListener.hpp>
#include <odin/md/MktDataListenerFactory.hpp>

#include <iostream>


class MDP3_MktDataListenerFactory
    : public odin::md::MktDataListenerFactory
{
public:

    MDP3_MktDataListenerFactory(std::ostream & = std::cout);

    virtual std::shared_ptr<odin::md::MktDataListener> create(int32_t a_secid) override;
    virtual std::shared_ptr<odin::md::MktDataListener> create(uint32_t a_secid) override;
    virtual std::shared_ptr<odin::md::MktDataListener> create(int64_t a_secid) override;

private:

  std::ostream & m_os;
};

