#include "MDP3MktDataListenerFactory.hpp"

#include "MDP3MktDataListener.hpp"

#include <odin/md/mdp3/feed/SecurityDefinitionStore.hpp>
#include <odin/md/mdp3/feed/SecurityID.hpp>

#include <iostream>
#include <stdexcept>


MDP3_MktDataListenerFactory::
MDP3_MktDataListenerFactory(std::ostream & os)
    : m_os(os)
{
}

std::shared_ptr<odin::md::MktDataListener>
MDP3_MktDataListenerFactory::
create(int32_t a_secid)
{
    return create(static_cast<int64_t>(a_secid));
}

std::shared_ptr<odin::md::MktDataListener>
MDP3_MktDataListenerFactory::
create(uint32_t a_secid)
{
    return create(static_cast<int64_t>(a_secid));
}

std::shared_ptr<odin::md::MktDataListener>
MDP3_MktDataListenerFactory::
create(int64_t a_secid)
{
    auto secdef = odin::md::mdp3::SecurityDefinitionStoreSingleton::instance().find(odin::md::mdp3::SecurityID(a_secid));
    if (not secdef) {
        throw std::logic_error("Failed to create MktDataListener");
    }
    return std::make_shared<MDP3_MktDataListener>(m_os);
}

