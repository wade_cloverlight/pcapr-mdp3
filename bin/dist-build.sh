#!/bin/bash

ARCH=${ARCH:-sandybridge}
TUNE=${TUNE:-$ARCH}
JOBS=${JOBS:-7}

while getopts a:t:g:j: opt; do
    case $opt in
	a)
	    ARCH=$OPTARG
	    ;;
	t)
	    TUNE=$OPTARG
	    ;;
	g)
	    GENFLAGS_OVERRIDE=$OPTARG
	    ;;
	j)
	    JOBS=$OPTARG
	    ;;
    esac
done

GENFLAGS=${GENFLAGS_OVERRIDE:-"-march=$ARCH -mtune=$TUNE -m64 -g -std=c++14 -Werror -Wall"}

prj_dir=$(cd `dirname $0`/.. && pwd)
cd $prj_dir

make clobber

dists="CME MDP3"
for var in $dists
do
    echo "==========================="
    echo "= Making dist variant: $var"
    echo "==========================="
    set -x
    make VARIANT=dbg GENFLAGS="$GENFLAGS" DIST=$var dist -j$JOBS
    make VARIANT=opt GENFLAGS="$GENFLAGS" DIST=$var dist -j$JOBS
    set +x
done


