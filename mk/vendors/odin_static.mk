
ifndef VLIBS
	ifndef VLIB
		err := $(error No static vendor library specified in VLIB or VLIBS!)
	endif
	VLIBS := $(VLIB)
endif

ifndef ODIN_ROOT
	err := $(error ODIN_ROOT not set!)
endif


ifndef ODIN_INCL
	ODIN_INCL := $(ODIN_ROOT)/include
endif
ifndef ODIN_LIB
	ODIN_LIB := $(ODIN_ROOT)/lib
endif

ifeq ($(filter $(ODIN_INCL),$(CXX_VENDOR_INCLS)),)
	CXX_VENDOR_INCLS += $(ODIN_INCL)
endif

ifeq ($(filter $(ODIN_LIB),$(CXX_LIBDIRS)),)
	CXX_LIBDIRS += $(ODIN_LIB)
endif

VLIBS := $(addsuffix -$(variant),$(addprefix odin_,$(VLIBS)))
_MISSING_VLIBS := $(filter-out $(CXX_SLIBS),$(VLIBS))
ifdef _MISSING_VLIBS
	CXX_SLIBS += $(_MISSING_VLIBS)
	_MISSING_VLIBS:=
endif

VLIBS:=
VLIB:=
