#!/usr/bin/env bash

# Compile and save output in make.out but show the first several lines of the output in case of errors.
clear; make -j16 2>&1 |tee make.out; echo; echo; head -15 make.out
