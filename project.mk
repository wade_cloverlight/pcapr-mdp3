ifndef CXX
    CXX := g++-6
endif

DFLT_GENFLAGS := -march=sandybridge -mtune=sandybridge -m64 -g -std=c++14 -Werror -Wall
DFLT_GENFLAGS_DBG := -O0
DFLT_GENFLAGS_OPT := -O3 -fno-trapping-math -fno-signaling-nans -fno-rounding-math
DFLT_LDFLAGS := -static-libgcc -static-libstdc++ -pthread
DFLT_CPPFLAGS_OPT := -DNDEBUG=1

PROJECT := pcapr
DIST_MDP3_PROJECT := pcapr-mdp3

